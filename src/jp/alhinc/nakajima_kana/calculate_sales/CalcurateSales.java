package jp.alhinc.nakajima_kana.calculate_sales;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CalcurateSales {

	public static void	 main(String[] args) {

		// コマンドライン引数でディレクトリを指定
		System.out.println("ここにあるファイルを開きます=>" + args[0]);


		BufferedReader br = null ;
		try {
			File file = new File(args[0], "branch.lst"); //支店定義ファイルを読み込む
			FileReader  fr = new FileReader(file) ;  //FileReaderクラスでファイルを読み込む
			br = new BufferedReader(fr);

			String line ;
			while ((line = br.readLine()) != null){
				System.out.println(line);
			}

		}catch(IOException e){
			System.out.println("エラーが発生しました。");

		}finally {

			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("closeできませんでした。");

				}

			}
		}


		//Fileクラスのオブジェクトを生成し対象のディレクトリを指定
		File dir = new File("C:/pleiades-2018-09-java-win-64bit-jre_20181004/pleiades/workspace/CalculateSales");
		File[] list = dir.listFiles();          //listFilesを使用してファイル一覧を取得


		for(int i=0; i<list.length; i++) {

			//ファイル名が.rcd拡張子かつ数字8桁のファイルを探す
			if( (list[i].getName().matches(".+rcd$")) && (list[i].getName().matches("^.{12}"))) {

				//System.out.println(list[i].getName()); //売上ファイルのファイル名を表示

				//支店コード、売上額を抽出する。
				//抽出した売上額を該当する支店の合計金額にそれぞれ加算する。
				//それを売上ファイルの数だけ繰り返す。

				try {
					File file = new File(list[i].getName(), ""); //支店定義ファイルを読み込む
					FileReader  fr = new FileReader(file) ;  //FileReaderクラスでファイルを読み込む
					br = new BufferedReader(fr);

					String line ;
					while ((line = br.readLine()) != null){
						System.out.println(line);


					//数字のみ、3桁固定を支店コードとして抽出

					if (line.matches("^[0-9]{3}")&& (line.matches("^(0[0-9]*)$"))) {

					System.out.println("支店コード：" + line);
					}


					//数字のみ、最大10桁を売上額として抽出
					if (line.matches("^\\d{10,}$")) {

					System.out.println("売上額：" + line);

					}

					}

				}catch(IOException e){
					System.out.println("エラーが発生しました。");

				}finally {

					if(br != null) {
						try {
							br.close();
						}catch(IOException e) {
							System.out.println("closeできませんでした。");

						}

					}
				}




			}



		}
	}
}

			/*
			BufferedReader br = null ;
			try {
				File file = new File(args[0], list[i].getName()); //xxxxx.rcd(売上ファイル)をfile変数に代入
				FileReader fr= new FileReader(file);
				br = new BufferedReader(fr);

				String line ;
				while ((line = br.readLine()) != null){
					System.out.println(line);

				}

			} catch(IOException e){
				System.out.println("エラーが発生しました。");

			}finally {

			if(br != null) {

				try {
					br.close();
				}catch (IOException e) {
							System.out.println("closeできませんでした。");

				}

			}
			*/














	//拡張子がrcd、且つファイル名が数字8桁のファイルを検索


        // ディレクトリの中のファイルがrcdで終わるかどうかを調べる
		// strが数字から始まる8桁の数字どうかを調べる




		//売上げファイルを読み込み、支店コード、売上額を抽出する

		//抽出した売上額を該当する支店の合計金額にそれぞれ加算し、売上フォルダの数だけ繰り返す

		//コマンドライン引数で指定されたディレクトリに支店別集計ファイルを作成

		//支店別集計ファイルのフォーマットに従い、全支店の支店コード、支店名、合計金額を出力





				//インスタンス化


				//シナリオ

